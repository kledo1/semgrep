package plugin

import (
	"os"
	"path/filepath"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/plugin"
)

// Match checks if any file extensions match existing supported languages
func Match(path string, info os.FileInfo) (bool, error) {
	ext := filepath.Ext(info.Name())
	if ext == ".py" || ext == ".js" || ext == ".ts" ||
		ext == ".jsx" || ext == ".tsx" || ext == ".c" || ext == ".go" {
		return true, nil
	}
	return false, nil
}

func init() {
	plugin.Register("semgrep", Match)
}
